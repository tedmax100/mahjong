package player

type Player struct {
	ID           uint64
	NickName     string
	Banker       bool
	DealerWind   uint8
	WinLosePoint int32
	Balance      int32
}
