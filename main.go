package main

import (
	"fmt"

	"gitlab.com/mahjong/point"
	"gitlab.com/mahjong/tile"
)

var Tiles [43]tile.Tile

var Groups []Group

type Group struct {
	Arr   []int
	Count int
}

const (
	// 0-8 萬
	W1 tile.Tile = iota
	W2
	W3
	W4
	W5
	W6
	W7
	W8
	W9
	// 9 - 17 筒
	TO1
	TO2
	TO3
	TO4
	TO5
	TO6
	TO7
	TO8
	TO9
	// 18- 26 條
	TI1
	TI2
	TI3
	TI4
	TI5
	TI6
	TI7
	TI8
	TI9
	// 27 - 30 東南西北
	HEast
	HSouth
	HWest
	HNorth
	// 31 - 33 中發白
	DZhong
	DFa
	DBai
	// 34-41春夏秋冬梅蘭竹菊
	FChun
	FXia
	FQiu
	FDong
	FMei
	FLan
	FZhu
	FJu
)

/* var (
	BigFourWind // 大四喜
	SmallFourWinds // 小四喜
) */

func main() {
	fmt.Println(W1)
	fmt.Println(FChun)
	fmt.Println(FJu)
	cs1 := []tile.Tile{
		//34,
		// 35, 36, 37, 38, 39, 40, 41,
		//0, 0,
		1, 2, 3,
		4, 5, 6,
		9, 10, 11,
		12, 13, 14,
		18, 19, 20,
		27, 27,
	}

	result := point.ClassificationHandPai(cs1)
	point.CanHuPai(result)
	fmt.Println(result)
	/* cs11 := Classification(cs1)
	gp := &Group{}
	Groups = make([]Group, 0)
	for i := range cs11 {
		if cs11[i] > 0 {
			gp.Arr = append(gp.Arr, int(cs11[i]))
			gp.Count += int(cs11[i])
		}
		if i > 26 || cs11[i+1] == 0 || i%9 == 8 {
			if gp.Count > 0 {
				Groups = append(Groups, Group{Arr: gp.Arr, Count: gp.Count})
				gp.Arr = []int{}
				gp.Count = 0
			}
		}
	}
	fmt.Println(CanHuPai(cs11))
	fmt.Println("groups:", Groups)
	hasPair := HasPair(Groups)
	fmt.Println(hasPair) */
}

func Classification(handTiles []tile.Tile) [43]tile.Tile {
	var result [43]tile.Tile

	for _, v := range handTiles {
		result[v]++
	}
	return result
}

func CheckWin(tiles []tile.Tile) bool {
	// 檢查是否有滿足花牌規則
	return true
}

func CheckFlowerWin(tiles [43]tile.Tile) bool {
	// 檢查是否八仙過海 或 七搶壹
	if tiles[34] == 1 && tiles[35] == 1 && tiles[36] == 1 && tiles[37] == 1 && tiles[38] == 1 && tiles[39] == 1 && tiles[40] == 1 && tiles[41] == 1 {
		return true
	}

	// 判斷正常牌型

	return false
}

func CanHuPai(tiles [43]tile.Tile) bool {
	if CheckFlowerWin(tiles) == true {
		return true
	}
	return CanHuPaiNorm(tiles)
}

func CanHuPaiNorm(tiles [43]tile.Tile) bool {
	count := 0
	for i := 0; i < 34; i++ {
		count += int(tiles[i])
	}

	// 剃除對子
	ret := false

	for i := 0; i < 34; i++ {
		// 剛好這牌就2張
		if tiles[i] == 2 {
			if CancutPair_2(tiles, i) == true {
				// 該牌數量少2
				tiles[i] -= 2

				ret = CanHuPai3N_Recursive(tiles, count-2, 0)
				tiles[i] += 2
				return ret
			} else {
				tiles[i] -= 2
				ret = CanHuPai3N_Recursive(tiles, count-2, 0)
				tiles[i] += 2
				if ret == true {
					return ret
				}
			}
		} else if tiles[i] == 3 { // 判斷周圍是否可以滿足組成一個順子
			if CancutPari3(tiles, i) == true {
				tiles[i] -= 2
				ret = CanHuPai3N_Recursive(tiles, count-2, 0)
				tiles[i] += 2
				if ret == true {
					return ret
				}
			}
		} else if tiles[i] == 4 {
			if CancutPair4(tiles, i) == true { // 判判拔掉該牌的其中兩張, 能不能跟周圍組成兩組順子; 如果不能組成順子就表示不能拆成2+2
				tiles[i] -= 2
				ret = CanHuPai3N_Recursive(tiles, count-2, 0)
				tiles[i] += 2
				if ret == true {
					return ret
				}
			}
		}
	}
	return ret
}

// 針對tiles[i] = 2 的情況進行剪枝處理
func CancutPair_2(tiles [43]tile.Tile, i int) bool {
	if i > 26 { // 風牌, 只能成對,刻, 不用考慮順
		return true
	} else if i == 0 || i == 9 || i == 18 { // 首張, 看下兩張數量
		if tiles[i+1] >= 2 && tiles[i+2] >= 2 { // 因為下兩張都>=2, 可能可以湊成順
			return false
		} else { // 該張牌的兩張就是對子
			return true
		}
	} else if i == 8 || i == 17 || i == 26 { // 該花色的末張
		if tiles[i-1] >= 2 && tiles[i-2] >= 2 { // 看前兩張數量都>=2, 可能可以湊成順
			return false
		} else { // 該張牌的兩張就是對子
			return true
		}
	} else if i == 1 || i == 10 || i == 19 { // 該花色的第二張
		if tiles[i-1]+tiles[i+1]+tiles[i+2] >= 4 && tiles[i+1] >= 2 { // 看前一張和後兩張數量和>= 4 且 下一張數量大於>=2 , 可能可以湊成順
			return false
		} else { // 該張牌的兩張就是對子
			return true
		}
	} else if i == 7 || i == 16 || i == 25 { // 該花色的倒數第二張
		if tiles[i-1]+tiles[i+1]+tiles[i-2] >= 4 && tiles[i-1] >= 2 {
			return false
		} else { // 該張牌的兩張就是對子
			return true
		}
	} else if tiles[i-1]+tiles[i+1]+tiles[i-2]+tiles[i+2] >= 4 { // 相鄰的兩端大於4張
		return false
	}
	return true

}

// 進行DP動態規劃
//  遞迴嘗試消除一組牌(3張), 當tiles所有值的count 都是0 , 就表示可以胡牌
// count表示剩餘牌數
// 當遇到衝突時, 不可胡牌
func CanHuPai3N_Recursive(tiles [43]tile.Tile, count, p int) bool {
	ret := false
	if count == 0 {
		return true
	}

	// 風、字 都只能組成刻
	if p > 26 {
		if tiles[p] >= 3 {
			ret = CanHuPai3N_Recursive(tiles, count-int(tiles[p]), p+1)
			return ret
		} else if tiles[p] == 0 {
			ret = CanHuPai3N_Recursive(tiles, count, p+1)
			return ret
		} else {
			return false
		}
	}

	if tiles[p] == 0 { // 如果該牌沒有了, 直接跳過進行下一張
		ret = CanHuPai3N_Recursive(tiles, count, p+1)
	}
	if tiles[p] == 1 {
		if (p % 9) > 6 { //如果該牌是8 or 9 無法組合成順子, 不能胡
			return false
		} else if tiles[p+1] > 0 && tiles[p+2] > 0 { // 能組成順
			tiles[p]--
			tiles[p+1]--
			tiles[p+2]--

			ret = CanHuPai3N_Recursive(tiles, count-3, p+1)

			tiles[p]++
			tiles[p+1]++
			tiles[p+2]++
		} else {
			return false
		}
	}

	if tiles[p] == 2 { // 能組成兩對順子
		if (p % 9) > 6 {
			return false
		} else if tiles[p+1] > 1 && tiles[p+2] > 1 { // 能組成順
			tiles[p] -= 2
			tiles[p+1] -= 2
			tiles[p+2] -= 2

			ret = CanHuPai3N_Recursive(tiles, count-6, p+1)

			tiles[p] += 2
			tiles[p+1] += 2
			tiles[p+2] += 2
		} else {
			return false
		}
	}

	if tiles[p] == 3 {
		ret = CanHuPai3N_Recursive(tiles, count-3, p+1) // 三對順子 == 三組碰
	}

	if tiles[p] == 4 { // 4張, 表示至少有一張跟後面的組成順, 剩下的繼續遞迴, p 不變
		if (p % 9) > 6 {
			return false
		} else if tiles[p+1] > 0 && tiles[p+2] > 0 {
			tiles[p]--
			tiles[p+1]--
			tiles[p+2]--

			ret = CanHuPai3N_Recursive(tiles, count-3, p)

			tiles[p]++
			tiles[p+1]++
			tiles[p+2]++
		} else {
			return false
		}
	}
	return ret
}

// 跟cutpair2 相反, 如果相鄰的牌< 2張, 就不可取
func CancutPari3(tiles [43]tile.Tile, i int) bool {
	if i > 26 {
		return false
	} else if i == 0 || i == 9 || i == 18 {
		if tiles[i+1] >= 1 && tiles[i+2] >= 1 { // 自己是3張, 2跟3都至少1張, 那自己是對子或者就是刻
			return true
		} else {
			return false
		}
	} else if i == 8 || i == 17 || i == 26 { // 7跟8都至少1張, 那自己是對子或者就是刻
		if tiles[i-1] >= 1 && tiles[i-2] >= 1 {
			return true
		} else {
			return false
		}
	} else if i == 1 || i == 10 || i == 19 {
		if tiles[i-1]+tiles[i+2] >= 1 && tiles[i+1] >= 1 { // 如果 1 + 4 >= 1張, 且  3 >=1; 1,222,3,4, 這種情況自己可能是對子
			return true
		} else {
			return false
		}
	} else if i == 7 || i == 16 || i == 25 {
		if tiles[i+1]+tiles[i-2] >= 1 && tiles[i-1] >= 1 { // 如果 9 + 6 >= 1張, 且  7 >=1;  這種情況自己可能是對子
			return true
		} else {
			return false
		}
	} else if tiles[i-1]+tiles[i+1]+tiles[i-2]+tiles[i+2] >= 2 { // 相鄰兩端大於2張牌
		return true
	}
	return false

}

// 針對tiles[i] = 4 的情況進行剪枝處理, 與CancutPair3相似, 由於多出來2個, 所以如果相鄰牌數小於4張牌, 則不可取
func CancutPair4(tiles [43]tile.Tile, idx int) bool {
	if idx > 26 {
		return false
	} else if idx == 0 || idx == 9 || idx == 18 {
		// 一萬一餅一條
		if tiles[idx+1] >= 2 && tiles[idx+2] >= 2 {
			// 如果對應的2和3 都大於2張
			return true
		} else {
			return false
		}
	} else if idx == 8 || idx == 17 || idx == 26 {
		if tiles[idx-1] >= 2 && tiles[idx-2] >= 2 {
			// 如果對應的7和8 都大於2張
			return true
		} else {
			return false
		}
	} else if idx == 1 || idx == 10 || idx == 19 {
		if tiles[idx-1]+tiles[idx+2] >= 2 && tiles[idx+1] >= 2 {
			// 如果對應的1+4  >= 2張, 且對應的3  >= 2張
			return true
		} else {
			return false
		}
	} else if idx == 7 || idx == 16 || idx == 25 {
		if tiles[idx-2]+tiles[idx+1] >= 2 && tiles[idx-1] >= 2 {
			// 如果對應的6+9  >= 2張, 且對應的7  >= 2張
			return true
		} else {
			return false
		}
	} else if tiles[idx-1]+tiles[idx+1]+tiles[idx-2]+tiles[idx+2] >= 4 {
		// 如果相鄰的兩端  >= 4張
		return true
	}
	return false
}

func HasPair(groups []Group) bool {
	var result = false

	for _, group := range groups {
		if group.Count%3 == 1 {
			return false
		} else if group.Count%3 == 0 {
			if Allow0(group) == false {
				return false
			}
		} else if group.Count%3 == 2 && !result {
			if Allow2(group) == false {
				return false
			}
			result = true
			return true
		} else {
			return false
		}
	}
	return result
}

func Allow0(group Group) bool {
	for i := range group.Arr {
		c := group.Arr[i] % 3

		if i > (len(group.Arr)-3) && c != 0 {
			return false
		}

		switch c {
		case 0:
			break
		case 1:
			if group.Arr[i+1] >= 1 && group.Arr[i+2] >= 1 {
				group.Arr[i+1] -= 1
				group.Arr[i+2] -= 1
			} else {
				return false
			}
			break
		case 2:
			if group.Arr[i+1] >= 2 && group.Arr[i+2] >= 2 {
				group.Arr[i+1] -= 2
				group.Arr[i+2] -= 2
			} else {
				return false
			}
			break
		}
	}
	return true
}

func Allow2(group Group) bool {
	// 數量剛好是3n+2, 看看有沒有滿足順子
	for i, v := range group.Arr {
		if v >= 2 {
			group.Arr[i] -= 2
			group.Count -= 2
			ret := Allow0(group)
			group.Arr[i] += 2
			group.Count += 2
			if ret == true {
				return true
			}
		}
	}
	return false
}
