package point

import (
	"fmt"

	"gitlab.com/mahjong/tile"
)

type Pai struct {
	Tile         tile.Tile
	Owner        uint8
	SelfGun      bool
	IdxThisMatch uint8
}

type Group struct {
	Arr   [][]Pai
	Count int
}

var Groups []Group

type MapPai [42][]Pai //map[tile.Tile][]Pai

func ClassificationHandPai(handTiles []tile.Tile) []Group {
	var paiMap MapPai = [42][]Pai{[]Pai{}}
	for i := range handTiles {
		// fmt.Println(len(paiMap[i]))
		/* 		if paiMap[handTiles[i]] == nil {
		   			paiMap[handTiles[i]] = []Pai{}
		   		}
		   		if (len(paiMap[handTiles[i]]) == 0) == true {
		   			paiMap[i] = []Pai{}
		   		} */
		paiMap[handTiles[i]] = append(paiMap[handTiles[i]], Pai{
			Tile: handTiles[i],
		})
	}

	gp := &Group{}
	// result := make([]Group, 0)
	for k := range paiMap {
		if len(paiMap[k]) > 0 {
			gp.Arr = append(gp.Arr, paiMap[k])
			gp.Count += int(len(paiMap[k]))
		}
		if k > 26 || len(paiMap[k+1]) == 0 || k%9 == 8 {
			if gp.Count > 0 {
				Groups = append(Groups, Group{Arr: gp.Arr, Count: gp.Count})
				gp.Arr = [][]Pai{}
				gp.Count = 0
			}
		}
	}
	return Groups
}

func CanHuPai(handPaiGroups []Group) bool {
	if CheckFlowerWin(handPaiGroups) == true {
		return true
	}
	return CanHuPaiNorm(handPaiGroups)
}

// 檢查是否八仙過海 或 七搶壹
func CheckFlowerWin(handPaiGroups []Group) bool {
	var flowerLen int
	for idx := range handPaiGroups {
		// if handPaiGroups[idx].Arr
		for iarr := range handPaiGroups[idx].Arr {
			if handPaiGroups[idx].Arr[iarr][0].Tile >= 34 {
				flowerLen = len(handPaiGroups) - idx
				if flowerLen == 8 {
					return true
				}
				return false
			}
		}
	}
	return false
}

func CanHuPaiNorm(handPaiGroups []Group) bool {
	count := 0
	// 計算非花牌的數量
	for idx := range handPaiGroups {
		// if handPaiGroups[idx].Arr
		if handPaiGroups[idx].Arr[0][0].Tile < 34 {
			count += handPaiGroups[idx].Count
		}
	}

	// 找出對子並且扣掉; 再來判斷順子
	result := false
	for i := 0; i < len(handPaiGroups); i++ {
		fmt.Println(CutPair(handPaiGroups[i]))
	}
	return result
}

func CutPair(group Group) bool {
	var hasPair = false
	if group.Count%3 == 1 {
		return false
	} else if group.Count%3 == 0 {
		if Allow0(group) == false {
			return false
		}
	} else if group.Count%3 == 2 && !hasPair {
		if Allow2(group) == false {
			return false
		}
		hasPair = true
		return true
	} else {
		return false
	}
	return hasPair
}

func Allow0(group Group) bool {
	for i := range group.Arr {
		c := group.Count % 3

		if i > (len(group.Arr)-3) && c != 0 {
			return false
		}

		switch len(group.Arr[i]) {
		case 0:
			break
		case 1:
			if len(group.Arr[i+1]) >= 1 && len(group.Arr[i+2]) >= 1 {
				// 把該張, 該下兩張, 拔出來湊順. (未來須考慮是同屬性)
				group.Arr[i+1] = group.Arr[i+1][1:]
				group.Arr[i+2] = group.Arr[i+2][1:]

				group.Arr[i] = group.Arr[i][1:]
			} else {
				return false
			}
			break
		case 2:
			if len(group.Arr[i+1]) >= 2 && len(group.Arr[i+2]) >= 2 {
				// 把該張, 該下兩張, 拔出來湊順. (未來須考慮是同屬性)
				group.Arr[i+1] = group.Arr[i+1][2:]
				group.Arr[i+1] = group.Arr[i+2][2:]

				group.Arr[i] = group.Arr[i][2:]
			} else {
				return false
			}
			break
		}
	}
	return true
}

func Allow2(group Group) bool {
	for i := range group.Arr {
		if len(group.Arr[i]) >= 2 {
			copyGroup := make([]Pai, len(group.Arr[i]))
			copy(copyGroup, group.Arr[i])
			group.Arr[i] = group.Arr[i][2:]
			group.Count -= 2
			ret := Allow0(group)
			group.Arr[i] = copyGroup
			group.Count += 2

			if ret == true {
				return true
			}
		}
	}
	return false

}
