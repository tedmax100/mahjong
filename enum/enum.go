package enum

const (
	Self uint8 = iota
	Other

	MingGun bool = true
	AnGun   bool = false
)
