package main

import (
	"testing"

	. "gitlab.com/mahjong/tile"
)

func TestHuPai(t *testing.T) {
	var testCases = []struct { // Test table
		in  []Tile
		out bool
	}{
		{
			in: []Tile{
				27, 27, 27, 27,
				28, 28, 28,
				29, 29, 29,
				30, 30, 30,
				31, 31,
				32, 32, 32,
			},
			out: true,
		},
		{
			in: []Tile{
				0, 0,
				1, 2, 3,
				4, 5, 6,
				9, 10, 11,
				12, 13, 14,
				18, 19, 20,
			},
			out: true,
		},
		{
			in: []Tile{
				34, 35, 36, 37, 38, 39, 40, 41,
				0, 0,
				1, 2, 3,
				4, 5, 6,
				9, 10, 11,
				12, 13, 14,
				18, 19,
			},
			out: true,
		}, {
			in: []Tile{
				34, 35, 36, 37, 38, 39, 40,
				0, 0,
				1, 2, 3,
				4, 5, 6,
				9, 10, 11,
				12, 13, 14,
				18, 19, 20,
			},
			out: true,
		}, {
			in: []Tile{
				34, 35, 36, 37,
				0, 0,
				1, 2, 3,
				4, 5, 6,
				4, 5, 6,
				4, 5, 6,
				4, 5, 6,
			},
			out: true,
		}, {
			in: []Tile{
				34, 35, 36, 37,
				0, 0,
				4, 5, 6,
				4, 5, 6,
				4, 5, 6,
				4, 5, 6,
				4, 5, 6,
			},
			out: false,
		},
	}

	for _, testcase := range testCases {
		result := Classification(testcase.in)

		win := CanHuPai(result)
		if win != testcase.out {
			t.Fail()
		}
	}
}
